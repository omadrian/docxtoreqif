# docxtoreqif

Little project to capture all the word document content in a reqif(z) format model.

---

## Requirements
>(ironically captured in nor MSWord or ReqIF)

| Requirement ID | Description | Satisfaction Argument |
| -------------- |:-----------:| ---------------------:|
| 1              | The tool shall convert tables into rich text tables to be viewed in the reqifz model. | |
| 2              | The tool shall retain images in a subdirectory for reference by the ReqIF - to be viewed in the reqifz model. | |
| 3              | The tool shall preserve formatting as best as it can (size, bold text, italics, bullet points, numbered lists). | |
| 4              | The tool shall create a version for the imported requirements, based on a configurable MSWord field name, or if not found then a default numbering scheme starting at 1.0 and incrementing +1.0 at each updated version. | |
| 4.1            | The tool shall identify updated requirements by checking the decsription text of requirements with the same ID. | |
| 4.2            | The tool shall identify updated requirements by checking the requirement ID of requirements with the same description text. This option is ony available in a non-sctrict mode. | |
| 5              | The tool shall provide a CLI rich enough for the user to change the option values using arguments. |  |
| 6              | The tool shall provide an optional output format of reqif or reqifz. Rich text features are only available for reqifz. ||
| 7              | The tool shall preserve MSWord equations rendered correctly in rich text. ||
| 8              | The tool shall operate on batches of documents, creating { a single reqif(z) specification for each **OR** a single reqif(z) file and specificaiton or each **OR** a single reqif(z) file and specification containing all of the document content.}. The default generation mode shall be distinct specifications within  a single file. |  |
| 9              |  | |


---

## Dependancies

 * Python 2.7.13 used for development and test - 2.7.x should be sufficient.
 * python-docx package
 ```pip install python-docx```
 * pytest package
 ```pip install pytest```
 * pylint package recommended
 ```pip install pylint```

