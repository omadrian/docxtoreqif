"""
Main program for running the docx to reqif converter tool.
This main program handles arguments and runs the conversion.
"""

import docx
import argparse

# Set default tool options.
options = {
   "output_format"   : ".reqifz",
   "output_dir"      : "./gen",
   "input_file"      : "../test/system/inputs/Adrian - Summer Project.docx",
   "input_dir"       : "../test/system/inputs",
   "spec_generation" : "distinct-spec",
}


def add_args(parser):
   """
   """
   parser.add_argument("--reqif", 
                       help="Generate without rich text and images ReqIF format.")
   parser.add_argument("-p", "--path", type=str,
                       help="Relative or absolute path to a .docx file (with or without extension).")
   parser.add_argument("-d", "--dir", type=str,
                       help="Relative or absolute path to a directory containing .docx files (batch mode only).")
   parser.add_argument("-b", "--batch",
                       help="Use batch mode.")
   parser.add_argument("-o", "--output_dir", type=str,
                       help="Provide relative or absolute output path to a directory.")
   parser.add_argument("-s", "--spec_generation", 
                       choices=["single", "distinct-spec", "distinct-file"],
                       help="How many files and specificaitons to generation (batch mode only). Default is distinct-spec.")
   #parser.add_argument("strict",
   #                    help="Check requirements with the same discription text but different requirement IDs.")
   return parser

def parse_args(parser):
   args = parser.parse_args()
   if args.reqif is not None:
      options["output_format"] = ".reqif"
   if args.path is not None:
      options["input_file"] = args.path
   if args.dir is not None:
      options["input_dir"] = args.dir
   if args.output_dir is not None:
      options["output_dir"] = args.output_dir
   if args.spec_generation == "single":
      options["spec_generation"] = "single"
   elif args.spec_generation == "distinct-file":
      options["spec_generation"] = "distinct-file"
   elif args.spec_generation == "distinct-spec":
      options["spec_generation"] = "distinct-spec"

class DocxToReqif(object):
   """docstring for DocxToReqif"""
   def __init__(self, arg):
      super(DocxToReqif, self).__init__()
      self.arg = arg

   def extract_docx_content(self):
      """ Extract all the document content by paragraph, table and figure.
          This will likely result in tables separated from descriptive prose.
      """
      if options["input_file"] is not None:
         the_doc = docx.Document(options["input_file"])
         print the_doc

         # All Sections - the larger font will be maintained and used put into
         # the reqif.
         print "- Sections -"
         for sec in the_doc.sections:
            #print sec
            #print 80*"-"
         # All paragraphs.
         # TODO - there are unicode errors for some chars.
         print "- Paragraphs -"
         for para in the_doc.paragraphs:
            try:
               print para.text
            except (UnicodeEncodeError):
               pass
            print 80*"-"
         # All tables.
         print "- Tables -"
         for table in the_doc.tables:
            print table.rows
            print table.columns
            print 80*"-"
         # All figures.
         print "- Figures -"




   def format_tables(self):
      """ The table objects will be translated into the rich text
          format so that it can be displayed in a reqiq editor.
      """

   def write_figures(self):
      """ The figures will be translated into suitable xml to be displayed in 
          a reqif editor. 
      """

   def format_content_to_spec_hierarchies(self):
      """
      """

   def link_spec_hierarchies_with_objects(self):
      """
      """

   def write_to_specification(self):
      """
      """

   def write_to_reqif_content(self):
      """
      """
      
      

def main():
   """
   """
   # Parse args, and gather into options dict.
   parser = add_args(argparse.ArgumentParser())
   parse_args(parser)
   print options
   translator = DocxToReqif(options)
   translator.extract_docx_content()



if __name__ == "__main__":
   main()
